class PagesController < ApplicationController
    def index
        logo = Category.find_by(name: 'logo_nuevo')
        @logo = Post.find_by(category_id: logo.id)
        @posts = Post.all
        inicio = Category.find_by(name: 'inicio')
        @first = Post.find_by(category_id: inicio.id)
        @categories = Category.all
        finde = Category.find_by(name: 'vision')
        @about = Post.find_by(category_id: finde.id)
    end 

    def show
        logo = Category.find_by(name: 'logo_nuevo')
        @logo = Post.find_by(category_id: logo.id)
        @post = Post.find(params[:id])
        respond_to do |format|
            format.html
            format.json
        end
    end 
end
