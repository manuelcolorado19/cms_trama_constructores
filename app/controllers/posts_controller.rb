class PostsController < ApplicationController

    def index
        @posts = Post.all         
    end

    def new 
        @posts = Post.all     
        @post = Post.new
    end 

    def edit
        @post = Post.find params[:id]
    end

    def update

    end

    def create 
        @post = Post.new post_params
        @post.save!
        redirect_to posts_path 
    end 

    def destroy
        Post.destroy(params[:id])
        redirect_to posts_path 
    end
    
    private 
    def post_params
      params.require(:post).permit(:title,:comment,:date,{images:[]}, :category_id)
    end

end
