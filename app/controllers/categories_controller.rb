class CategoriesController < ApplicationController
    def new 
        @category = Category.new
        @categories = Category.all
    end

    def create 
        @post = Category.new category_params
        @post.save!
        redirect_to new_category_path 
    end 

    private 
    def category_params
      params.require(:category).permit(:name,:description,:kind)
    end
end
