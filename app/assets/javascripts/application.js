// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require trix
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require jquery
//= require jquery_ujs
//= require dropzone
//= require bootstrap

$(document).ready(function () {
  



  document.addEventListener('turbolinks:load',function(){

    $(".filter-button").click(function(){
      var value = $(this).attr('data-filter');
      
      if(value == "all")
      {
          //$('.filter').removeClass('hidden');
          $('.filter').show('1000');
      }
      else
      {
  //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
  //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
          $(".filter").not('.'+value).hide('3000');
          $('.filter').filter('.'+value).show('3000');
          
      }
  });
  if ($(".filter-button").removeClass("active")) {
    $(this).removeClass("active");
    }
    $(this).addClass("active");

    

    Array.from(document.getElementsByClassName("gallery_product")).forEach(function(element){
      element.childNodes[1].childNodes[3].classList.remove("opacity")
      element.childNodes[1].childNodes[1].addEventListener("mouseover", function(){
        this.parentNode.childNodes[3].classList.add("opacity") 
      })

      element.childNodes[1].childNodes[1].addEventListener("mouseout", function(){
        this.parentNode.childNodes[3].classList.remove("opacity") 
      })
    })

    var sections = $('section')
    , nav = $('nav')
    , navheight = nav.outerHeight();
    nav.find('a').addClass('active-white')

  $(window).on('scroll', function(){
    var current_pos = $(this).scrollTop();
    sections.each(function(){
      var top = $(this).offset().top - navheight,
      bottom = top + $(this).outerHeight()
      if(current_pos >= top && current_pos <= bottom){
        if ($(this).attr('id')!=1){
          nav.addClass('bg-light')
          nav.find('a').removeClass('active-white')
          nav.find('a').addClass('active-black')
          nav.find('a').removeClass('active-link')
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active-link')
        }else{
          nav.removeClass('bg-light')
          nav.find('a').removeClass('active-link')
          nav.find('a').removeClass('active-black')
          nav.find('a').addClass('active-white')
        }
      }
    }) 
  })

  nav.find('a').on('click', function(){
    var $n = $(this)
    m = $n[0].getAttribute('href')
    $('html, body').animate({
      scrollTop: $(m).offset().top - navheight 
    }, 500)
    return false
  })

  })  
    
})


