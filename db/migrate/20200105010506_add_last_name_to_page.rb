class AddLastNameToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :last_name, :string
  end
end
