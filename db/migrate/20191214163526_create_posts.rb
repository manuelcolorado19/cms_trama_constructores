class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.text :title
      t.text :comment
      t.date :date
      t.text :image_url

      t.timestamps
    end
  end
end
