class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :name
      t.text :email
      t.text :detail
      t.text :number_phone

      t.timestamps
    end
  end
end
